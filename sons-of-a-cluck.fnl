;; title:   Sons of Cluck
;; author:  parlortricks
;; desc:    As the sons of a cluck, your mummy isn't around to feed you and you're very hungry!
;; site:    website link
;; license: GPL-3.0-or-later
;; SPDX-License-Identifier: GPL-3.0-or-later
;; version: 0.1
;; script:  fennel
;; strict:  true
;; input: gamepad
;; saveid: sons-of-cluck-1
;; Copyright (c) 2022 parlortricks

(macro inc! [v ...]
  (if (= ... nil)
      `(set ,v (+ ,v 1))
      `(set ,v (+ ,v (+ ,...)))))

(macro dec! [v ...]
  (if (= ... nil)
      `(set ,v (- ,v 1))
      `(set ,v (- ,v (+ ,...)))))

(local (screen-width screen-height) (values 240 136))
(local (rnd flr sin cos atan2) (values math.random math.floor math.sin math.cos math.atan2))
(local seed (rnd 0 100))
(local palette-map 0x3FF0)

(var (t dt pt) (values 0 0 0))
(var intro-timer 0)
(var intro-active false)

(var menu-player 1)
(var menu-flip false)
(var direction 1)

(var debug false)
(var fps {})

(var (tx tdx tdxs) (values 0 1 3)) ;;hills

(var (player1 player2) {})

(local FPS {:new (fn [self o]
                    (local o (or o {}))
                    (setmetatable o self)
                    (set self.__index self)
                    (set self.value 0)
                    (set self.frames 0)
                    (set self.last-time 0)
                    FPS)
            :get-value (fn [self]
                        (if (<= (- (time) self.last-time) 1000) 
                              (inc! self.frames)
                            (do
                              (set self.value self.frames)
                              (set self.frames 0)
                              (set self.last-time (time))))
                              self.value)})

(fn stats []
  (when debug
    (rect 0 0 screen-width 5 15)
    (let [fpsv (.. "FPS: " (fps:get-value))]
      (print fpsv (- (/ screen-width 2) (/ (* (length fpsv) 6) 2)) 0 11 false 1 false))))             

(fn scene-manager []
  (let [s {}]
    (set s.scenes {})
    (set s.current_scenes "")
    (set s.overlayer {})

    (fn s.add [self scene name]
      (tset s.scenes name scene))

    (fn s.active [self name]
      (set s.current_scene name)
      (: (. s.scenes s.current_scene) :on-active))

    (fn s.input [self]
      (: (. s.scenes s.current_scene) :input))

    (fn s.update [self]
      (: (. s.scenes s.current_scene) :update))

    (fn s.draw [self]
      (: (. s.scenes s.current_scene) :draw))

    s))

(local manager (scene-manager))    

(fn create-animation [frames size fps]
  {:cf 0 : frames : size :t (* (/ 1 fps) 1000) :spf (* (/ 1 fps) 1000)})

(fn reset-animation [anim]
  (set anim.cf 0)
  (set anim.t anim.spf))

(fn draw-animation [anim dt x y ts flip]
  (set anim.t (- anim.t dt))
  (when (<= anim.t 0)
    (set anim.t (+ anim.t anim.spf))
    (set anim.cf (+ anim.cf 1))
    (when (>= anim.cf (length anim.frames))
      (set anim.cf 0)))
  (local f (. anim.frames (+ anim.cf 1)))
  (for [i 0 anim.size 1]
    (for [j 0 anim.size 1]
      (if flip
          (spr (+ (+ f i) (* j 16)) (+ x (* (- anim.size i) 8)) (+ y (* j 8)) ts 1 1)
          (spr (+ (+ f i) (* j 16)) (+ x (* i 8)) (+ y (* j 8)) ts 1 0)))))

(fn print-center [str x y color scale]
  (let [pixel-width (* (print str -50 -50 -8) scale)]
    (print str (- x (/ pixel-width 2)) y color false scale)))

(fn print-center-outline [str x y color color2 scale]
  (let [pixel-width (* (print str -50 -50 -8) scale)]
    (for [dx -1 1]
      (for [dy -1 1]
        (print str (+ (- x (/ pixel-width 2)) dx) (+ y dy) color2 false scale)))
    (print str (- x (/ pixel-width 2)) y color false scale)))  

(fn print-outline [str x y color color2 scale]
  (let [pixel-width (* (print str -50 -50 -8) scale)]
    (for [dx -1 1]
      (for [dy -1 1]
        (print str (+ x dx) (+ y dy) color2 false scale)))
    (print str x y color false scale)))      

(fn sprite-outline [i x y t s f r w h b col]
  (for [c 0 15]
    (poke4 (+ (* palette-map 2) c) col))
  (for [dx (- b) b]
    (for [dy (- b) b]
      (spr i (+ x dx) (+ y dy) t s f r w h)))
  (for [c 0 15]
    (poke4 (+ (* palette-map 2) c) c))
  (spr i x y t s f r w h))    

(fn sget [x y]
  (let [addr (+ 0x4000 (* (+ (// x 8) (* (// y 8) 16)) 32))]
  (peek4 (+ (* addr 2) (% x 8) (* (% y 8) 8)))))	  
 
(fn rand-real [a b]
	(* (+ a (- b a)) (math.random)))  

(var logo-frames 0)
(var logoanim 96)
(local logostop 127)
(local logo-sprite 128)  
(local (logo-width logo-height) (values 64 64))

(fn display-logo []
  (let [cx (-  (/ screen-width 2) (/ logo-width 2))
        cy (-  (/ screen-height 2) (/ logo-height 2))
        frx (* (% logo-sprite 16) 8)
        fry (* (flr (/ logo-sprite 16)) 8)]
    (for [s 0 logoanim 1]
      (for [x 0 63 1]
        (local camera-x (rand-real 0 (/ 30 logo-frames)))
        (local camera-y (rand-real 0 (/ 30 logo-frames)))
        (for [y 0 63 1]        
          (if (= (+ x y) s) 
                (pix (- (+ cx x) camera-x) (- (+ cy y) camera-y) 12)
              (= (+ x y) (- s 1)) 
                (pix (- (+ cx x) camera-x) (- (+ cy y) camera-y) 13)
              (< (+ x y) (- s 1))
                (pix (- (+ cx x) camera-x) (- (+ cy y) camera-y) (sget (+ frx x) (+ fry y)))))))

    (when (<= logoanim logostop)
      (set logoanim (+ logoanim 1)))
    (when (> logo-frames 35)
      (print-center "parlortricks 2022" 120 120 14 1)))) 

(fn sinw [pos period amp]
  (* (sin (* pos period)) amp))	

(fn topography [x]
  (let [pos (/ x 127)]
    (+ (+ (+ 90 (sinw (+ pos (* 100 seed)) 2 10)) 
          (sinw (+ pos (* 100 seed)) 0.8 10))
       (sinw (+ pos (* 100 seed)) 8 1))))         

(fn draw-hills []
  (for [x 0 239 1]
    (local y (topography (- tx x)))
    (line x 135 x (- 170 (topography (- x (/ tx 8)))) 15)
    (line x 135 x (topography (- tx x)) 14)
    (for [y (- y 1) (topography y) 1]
      (pix x y 13))))

(fn intro []
  (let [s {}]
    (fn s.on-active [self]
      (set intro-active true))

    (fn s.input [self]
      (when (or (btnp 4) (btnp 5))
        (manager:active :menu)))

    (fn s.update [self]
      (when (> intro-timer 60)
        (set intro-active false)
        (manager:active :menu))
      (set intro-timer (+ intro-timer 1))
      (set t (+ t (/ 1 30)))
      (set logo-frames (+ logo-frames 1)))

    (fn s.draw [self]
      (cls)
      (if intro-active
        (display-logo)))

    s))                

(fn game []
  (let [s {}]
    (fn s.on-active [self])

    (fn s.input [self]
      (if (not= (btn) 0)
      (do
      (when (btn 2)
        (set player1.flip true)
        (set player1.x (- player1.x 1))
        (set player2.flip true)
        (set player2.x (- player2.x 1))
        (set tx (+ tx (/ (* tdx tdxs) 8)))
        (set player1.walk true)
        (set player2.walk true)
        )
      (when (btn 3)
        (set player1.flip false)
        (set player1.x (+ player1.x 1))
        (set player2.flip false)
        (set player2.x (+ player2.x 1))
        (set tx (- tx (/ (* tdx tdxs) 8)))
        (set player1.walk true)
        (set player2.walk true)
        ))
        (do      
        (set player1.walk false)
        (set player2.walk false)))

      (if player1.walk (set player1.anim.active :walk) (set player1.anim.active :idle))
      (if player2.walk (set player2.anim.active :walk) (set player2.anim.active :idle)))


    (fn s.update [self])

    (fn s.draw [self]
      (cls 5)
      (draw-hills)
      (map 0 0 30 17 0 0 5)

      ;;(draw-animation (. player1.anim player1.anim.active) (* dt 1000) player1.x player1.y 5 player1.flip)
      (draw-animation (. player2.anim player2.anim.active) (* dt 1000) player2.x player2.y 5 player2.flip))

    s))

(fn menu []
  (let [s {}]
    (fn s.on-active [self]
      (set t 0))

    (fn s.input [self]
      (when (not= (btnp) 0) ;; anykey
        (manager:active :game)))

    (fn s.update [self]
      (if (= direction 1)
            (inc! t)
          (= direction 0)
            (dec! t)))

    (fn s.draw [self]
      (cls 5)

      (sprite-outline 448 (- (/ screen-width 2) 56) 30 5 1 0 0 14 4 1 0)
      (sprite-outline 494 164 54 5 1 0 0 2 2 1 0)
      ;(print-outline "Sons of a" 64 26 4 0 1)
      ;(print-center-outline "1 player" 120 80 3 0 1)
      (font "1 PLAYER" 88 70 5 8 7 true 1)
      ;(print-center-outline "2 player" 120 91 3 0 1)
      (font "2 PLAYER" 88 82 5 8 7 true 1)
      ;(print-center-outline "high score" 120 102 3 0 1)
      (font "HIGH SCORE" 80 94 5 8 7 true 1)

      (font "SONS OF A" 64 23 5 7 7 false 1)
      
      
      (if (= menu-player 1)
        (draw-animation player1.anim.walk (* dt 1000) t 112 5 menu-flip)
          (= menu-player 2)
        (draw-animation player2.anim.walk (* dt 1000) t 112 5 menu-flip))
      
      (if (and (= direction 1) (> t 239))
            (do
              (set t -32)
              (set menu-player (math.floor (math.random 1 2)))
              (set direction (math.floor (math.random 0 1)))
              (if (= direction 0)
                    (set menu-flip true)
                    (set menu-flip false)))
          (and (= direction 0) (< t -32))
            (do
              (set t 240)
              (set menu-player (math.floor (math.random 1 2)))
              (set direction (math.floor (math.random 0 1)))
              (if (= direction 0)
                    (set menu-flip true)
                    (set menu-flip false)))))

    s))        

(fn _G.BOOT []
  (set fps (FPS:new))
  (set player1 {:x 120
                :y 68
                :flip false
                :walk false
                :anim {:active :idle
                       :walk (create-animation [352 355 358 361 364 400] 2 16)
                       :idle (create-animation [364 400] 2 2)}})

  (set player2 {:x 108
                :y 96
                :flip false
                :walk false
                :anim {:active :idle
                       :walk (create-animation [0 3 6 9 12 48] 2 16)
                       :idle (create-animation [12 48] 3 2)}})  
  (manager:add (intro) :intro)
  (manager:add (menu) :menu)
  (manager:add (game) :game)
  (manager:active :intro))


;; gradient sky settings
(local pal-color 5)
(local PALADDR (+ 0x3FC0 (* pal-color 3))) 
(local (R1 G1 B1) (values 33 147 176))  ;;#2193b0
(local (R2 G2 B2) (values 109 213 237)) ;;#6dd5ed     

(fn _G.SCN [row]
  (when (or (= manager.current_scene :game) (= manager.current_scene :menu))
    (var (d hh h) nil)
    (set hh (- screen-height (* 8 4)))
    (set h (* hh 0.1))
    (if (< row h) 
          (set d 0)
        (> row hh) 
          (set d 1)
        (set d (/ (- row h) (- hh h))))
    (poke PALADDR (+ R1 (* (- R2 R1) d)))
    (poke (+ PALADDR 1) (+ G1 (* (- G2 G1) d)))
    (poke (+ PALADDR 2) (+ B1 (* (- B2 B1) d)))))

(fn _G.OVR []
  (stats))

(fn _G.TIC []
  (set dt (/ (- (time) pt) 1000))
  (set pt (time))
  (manager:input)
  (manager:update)
  (manager:draw)
  )

